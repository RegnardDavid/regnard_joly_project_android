package com.eilco.davidregnard.td1exo3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView number;
    int sum;
    char lastOP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lastOP=' ';

        number = (TextView)findViewById(R.id.number);

        final Button add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                compute(v);
                lastOP='+';
                number.setText(Integer.toString(0));
            }
        });

        final Button sub = findViewById(R.id.sub);
        sub.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                compute(v);
                lastOP='-';
                number.setText(Integer.toString(0));
            }
        });

        final Button div = findViewById(R.id.div);
        div.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                compute(v);
                lastOP='/';
                number.setText(Integer.toString(0));
            }
        });

        final Button times = findViewById(R.id.times);
        times.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                compute(v);
                lastOP='*';
                number.setText(Integer.toString(0));
            }
        });

        final Button equal = findViewById(R.id.equal);
        equal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                compute(v);
                number.setText(Integer.toString(sum));
                sum = 0;
            }
        });

        final Button clear = findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sum = 0;
                number.setText(Integer.toString(sum));
            }
        });
        Button numbers[] = new Button[10];
        numbers[0] = findViewById(R.id.zero);
        numbers[1] = findViewById(R.id.one);
        numbers[2] = findViewById(R.id.two);
        numbers[3] = findViewById(R.id.three);
        numbers[4] = findViewById(R.id.four);
        numbers[5] = findViewById(R.id.five);
        numbers[6] = findViewById(R.id.six);
        numbers[7] = findViewById(R.id.seven);
        numbers[8] = findViewById(R.id.eight);
        numbers[9] = findViewById(R.id.nine);

        for (final Button num:numbers) {
            num.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(number.getText().toString()==""||Integer.parseInt(number.getText().toString())==0){
                        if(number.getText().toString()==""){
                            sum=Integer.parseInt(num.getText().toString());
                        }
                        number.setText(num.getText().toString());
                    }else{
                        number.setText(number.getText().toString() + "" + num.getText().toString());
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void computeAdd(View v){
        int b=0;
        if(number.getText().toString()!=""){
            b=Integer.parseInt(number.getText().toString());
            sum +=b;
        }
    }

    public void computeSub(View v){
        int b=0;
        if(number.getText().toString()!="") {
            b = Integer.parseInt(number.getText().toString());
            sum -=b;
        }
    }

    public void computeDiv(View v){
        int b=0;
        if(number.getText().toString()!="") {
            b = Integer.parseInt(number.getText().toString());
            if(b!=0){
                sum /=b;
            }
        }
    }

    public void computeTimes(View v){
        int b=0;
        if(number.getText().toString()!="") {
            b = Integer.parseInt(number.getText().toString());
            sum *=b;
        }
    }
    public void compute(View v){
        switch (lastOP){
            case '+':
                computeAdd(v);
                break;
            case '-':
                computeSub(v);
                break;
            case '*':
                computeTimes(v);
                break;
            case '/':
                computeDiv(v);
                break;
            default:
                break;
        }
    }
}
