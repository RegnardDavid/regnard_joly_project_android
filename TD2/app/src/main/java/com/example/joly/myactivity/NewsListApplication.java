package com.example.joly.myactivity;

import android.app.Application;

public class NewsListApplication extends Application {

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String login;

    @Override
    public void onCreate()
    {
        super.onCreate();  this.login = null;  }
}
