package com.example.joly.myactivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity {

    private TextView login;
    private static final String TAG = "NewsList";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        login = findViewById(R.id.login);
        NewsListApplication app = (NewsListApplication) getApplicationContext();

        String logtext = "hello " + app.getLogin();

        login.setText(logtext);



        final Button button = findViewById(R.id.news);
        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String url = "http://android.busin.fr/";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                finish();
            }
        });

        final Button details = findViewById(R.id.details);
        details.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {

                Intent intent = new Intent(NewsActivity.this , DetailsActivity.class);
                startActivity(intent);
            }
        });

        final Button logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {

                Intent intent = new Intent(NewsActivity.this , LoginActivity.class);
                startActivity(intent);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
