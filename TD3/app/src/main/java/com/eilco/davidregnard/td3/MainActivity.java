package com.eilco.davidregnard.td3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView nRecyclerView;
    private List<JeuVideo> mesJeux;
    private MyVideosGamesAdapter monAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nRecyclerView = (RecyclerView)findViewById(R.id.myRecyclerView);

        mesJeux = new ArrayList<>();

        mesJeux.add(new JeuVideo("Forza Horizon 3", 49.90F));
        mesJeux.add(new JeuVideo("Forza Motorsport 5", 20.00F));
        mesJeux.add(new JeuVideo("Asseto Corsa", 22.00F));
        mesJeux.add(new JeuVideo("Grand Theft Auto 5", 49.90F));

        monAdapter = new MyVideosGamesAdapter(mesJeux);

        nRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        nRecyclerView.setAdapter(monAdapter);
    }
}
