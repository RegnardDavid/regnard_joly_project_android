package com.eilco.davidregnard.td3;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


public class MyViewHolder extends RecyclerView.ViewHolder {

    private TextView nNameTv;
    private TextView nPriceTv;

    public MyViewHolder(View itemView) {
        super(itemView);
        this.nNameTv = itemView.findViewById(R.id.name);
        this.nPriceTv =itemView.findViewById(R.id.price);
    }

    void display(JeuVideo jeuVideo){
        nNameTv.setText(jeuVideo.getName());
        nPriceTv.setText(jeuVideo.getPrice() + "€");
    }
}
